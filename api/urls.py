from django.urls import path, include
from rest_framework import routers
from .views import *

router = routers.DefaultRouter()
router.register('user', UserViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('user/<int: pk>', UpdateDeleteUser.as_view(), name='UpdateDeleteUser'),
    path('user/', CreateNewUserViewSet.as_view(), name='CreateNewUserViewSet'),
    path('masoud/', masoud),
]