from django.db import models


# Create your models here.

class User(models.Model):
    name = models.CharField(max_length=100)
    family = models.CharField(max_length=100)
    phone_name = models.CharField(max_length=20, blank=True)
    # created_at = models.DateTimeField(auto_now_add=True)
    # updated_at = models.DateTimeField(auto_now=True)


def __str__(self):
    return self.name + self.family
