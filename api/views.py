from django.shortcuts import render
from rest_framework import viewsets, status
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.generics import RetrieveUpdateDestroyAPIView, ListCreateAPIView

from api.models import User
from api.serializers import UserSerializers


# Create your views here.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializers


class UpdateDeleteUser(RetrieveUpdateDestroyAPIView):
    serializer_class = UserSerializers
    queryset = User.objects.all()

    def put(self, request, *args, **kwargs):
        pass

    def get_queryset(self, pk):
        try:
            movie = User.objects.get(pk=pk)
        except User.DoesNotExist:
            content = {
                'status': 'Not Found'
            }
            return Response(content, status=status.HTTP_404_NOT_FOUND)
        return movie

    # Get a movie
    def get(self, request, *args, **kwargs):
        print(' in get METHOD')
        # movie = self.get_queryset(pk)
        # serializer = UserSerializers(movie)
        # return Response(serializer.data, status=status.HTTP_200_OK)

    # Update a movie
    def put(self, request, pk):

        movie = self.get_queryset(pk)

        if request.user == movie.creator:  # If creator is who makes request
            serializer = UserSerializers(movie, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            content = {
                'status': 'UNAUTHORIZED'
            }
            return Response(content, status=status.HTTP_401_UNAUTHORIZED)

    # Delete a movie
    def delete(self, request, *args, **kwargs):
        movie = self.get_queryset(kwargs['pk'])
        print('i am here')
        if request.user == movie.creator:  # If creator is who makes request
            movie.delete()
            content = {
                'status': 'NO CONTENT'
            }
            return Response(content, status=status.HTTP_204_NO_CONTENT)
        else:
            content = {
                'status': 'UNAUTHORIZED'
            }
            return Response(content, status=status.HTTP_401_UNAUTHORIZED)


# CREATE AND LISTING

class CreateNewUserViewSet(ListCreateAPIView):
    serializer_class = UserSerializers

    permission_classes = (IsAuthenticated,)

    # pagination_class = CustomPagination

    def get_queryset(self):
        movies = User.objects.all()
        return movies

    def perform_create(self, serializer):
        print('new user created')

    # Get all movies
    def get(self, request, **kwargs):
        movies = self.get_queryset()
        print('i am here∞')
        paginate_queryset = self.paginate_queryset(movies)
        serializer = self.serializer_class(paginate_queryset, many=True)
        return self.get_paginated_response(serializer.data)

    def post(self, request, *args, **kwargs):
        serializer = UserSerializers(data=request.data)
        if serializer.is_valid():
            serializer.save()
    # Create a new movie
    # def post(self, request):
    #     serializer = UserSerializers(data=request.data)
    #     if serializer.is_valid():
    #         serializer.save(creator=request.user)
    #         return Response(serializer.data, status=status.HTTP_201_CREATED)
    #     return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def masoud(request):
    # for testing purpose
    print(' i am here')
    content = {
        'name': 'masoud heydari',
        'family': 'heydari'
    }
    return Response(content, status=status.HTTP_200_OK)
