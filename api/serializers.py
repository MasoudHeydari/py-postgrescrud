from rest_framework import serializers

from api.models import User


class UserSerializers(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = (
            'id',
            'name',
            'family',
            'url'  # this property exist in the 'HyperlinkModelSerializer'
        )

